import pygame

def img_next_bg(slika):
    if slika == bg_img1:
        slika = bg_img2
    elif slika == bg_img2:
        slika = bg_img3
    elif slika ==  bg_img3:
        slika = bg_img4
    elif slika ==  bg_img4:
        slika = bg_img5
    elif slika ==  bg_img5: 
        slika = bg_img1
    
    return slika

pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1000
HEIGHT = 1000

RED = [ 255, 0, 0 ]
GREEN = [ 0, 255, 0 ]

myfont = pygame.font.SysFont('Arial',40)
avion_text = myfont.render("Vozi se u Croatia Ejrlajnsu!", False, RED)
avioni_text = myfont.render("Iskusi ratne avione uz ministra obrane!!", False, RED)
nasa_text=myfont.render("Iskusi svemirske žuljeve uz nove NASA tenisice", False, GREEN)
piloti_text = myfont.render("Prijavi se u vojnu akademiju za pilota. Budi najbolji među najjačima.", False, RED)
ravna_text = myfont.render("ZemLjA jE rAvNa plOćA!", False, RED)

bg_img1 = pygame.image.load("avion.jpg")
bg_img1 = pygame.transform.scale(bg_img1, (WIDTH, HEIGHT))
bg_img2 = pygame.image.load("avioni.jpeg")
bg_img2 = pygame.transform.scale(bg_img2, (WIDTH, HEIGHT))
bg_img3 = pygame.image.load("nasa.jpg")
bg_img3 = pygame.transform.scale(bg_img3, (WIDTH, HEIGHT))
bg_img4 = pygame.image.load("piloti.jpg")
bg_img4 = pygame.transform.scale(bg_img4, (WIDTH, HEIGHT))
bg_img5 = pygame.image.load("ravna.jpg")


# tuple velicine prozora
size = (WIDTH, HEIGHT)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Reklame")

clock = pygame.time.Clock()
i = 0
duration = 0

done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                bg_color = next_bg(bg_color)
    #i += 1
    if duration < 0 and i == 0:
        duration = 10000
        screen.blit(bg_img1, (0,0))
        screen.blit(avion_text, (60,70))
        i = 1
    elif duration < 0 and i == 1:
        duration = 10000
        screen.blit(bg_img2, (0,0))
        screen.blit (avioni_text, (60,70))
        i = 2
    elif duration < 0 and i == 2:
        duration = 10000
        screen.blit(bg_img3, (0,0))
        screen.blit (nasa_text, (60,70))
        i = 3
    elif duration < 0 and i == 3:
        duration = 10000
        screen.blit(bg_img4, (0,0))
        screen.blit (piloti_text, (60,70))
        i = 4
    elif duration < 0 and i == 4:
        duration = 10000
        screen.blit(bg_img5, (0,0))
        screen.blit (ravna_text, (60,70))
        i = 5
    elif duration < 0 and i == 5:
        duration = 10000
        bg_img = img_next_bg(bg_img)
        i = 0

    pygame.display.flip()
    
    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    duration = duration-time
pygame.quit()