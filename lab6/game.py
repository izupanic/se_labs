# ukljucivanje biblioteke pygame
import pygame
 
def next_bg(color):
    if color == TIRKIZ:
        color = RED
    elif color == RED:
        color = MAGENTA
    elif color == MAGENTA:
        color = BLUE
    elif color == BLUE:
        color = TIRKIZ  
    return color
 
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900
 
RED = [ 255, 0, 0 ]
GREEN = [ 0, 255, 0 ]
BLUE = [ 0, 0, 255 ]
TIRKIZ = [ 64,224,208 ]
MAGENTA = [ 255, 0, 255 ]
YELLOW = [255, 255, 0]
BLACK = [0, 0, 0]
 
 
# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")
 
state= "red" #"yellow", "gereen"
clock = pygame.time.Clock()
i = 0
duration = 3000
bg_color = TIRKIZ
done = False
while not done:
    print(state)
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if state == "red":
                    state = "yellow"
                elif state == "yellow":
                    state ="green"   
                elif state == "green":
                    state = "red" 

    #state change
    if duration <=0:
        if state == "red":
            state = "redyellow"
            duration=1000
        elif state== "redyellow":
            state="green"
            duration=3000
        elif state=="green":
            state ="blinking"
            duration=1000
        elif state=="blinking":
            state ="yellow"
            duration=1000
        elif state =="yellow":
            state ="red"
            duration =3000

    #iscrtavanje
    if state == "red":
        screen.fill( RED )
    elif state== "redyellow":
        pygame.draw.rect(screen, RED, (0,0,WIDTH,HEIGHT/2))
        pygame.draw.rect(screen, YELLOW, (0,HEIGHT/2,WIDTH,HEIGHT))
    elif state =="yellow":
        screen.fill(YELLOW)
    elif state == "green":
        screen.fill(GREEN)
    elif state=="blinking":
        screen.fill(MAGENTA)
        if duration>=800:
            screen.fill(BLACK)
        elif duration >=600:
            screen.fill(GREEN)
        elif duration >=400:
            screen.fill(BLACK)
        elif duration >=200:
            screen.fill(GREEN)
        else:
            screen.fill(BLACK)
    pygame.display.flip()
    
    time = clock.tick(60)
    duration = duration-time
pygame.quit()