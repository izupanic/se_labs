# ukljucivanje biblioteke pygame
import pygame

def img_next_bg(slika):
    if slika == bg_img1:
        slika = bg_img2
    elif slika == bg_img2:
        slika = bg_img3
    elif slika ==  bg_img3:
        slika = bg_img4
    elif slika ==  bg_img4:
        slika = bg_img5
    elif slika ==  bg_img5:
        slika = bg_img6
    elif slika ==  bg_img6:
        slika = bg_img7
    elif slika ==  bg_img7:
        slika = bg_img8
    elif slika ==  bg_img8:
        slika = bg_img1
    return slika

pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1000
HEIGHT = 1000

bg_img1 = pygame.image.load("crveno.png")
bg_img1 = pygame.transform.scale(bg_img1, (WIDTH, HEIGHT))
bg_img2 = pygame.image.load("zutocrveno.png")
bg_img2 = pygame.transform.scale(bg_img2, (WIDTH, HEIGHT))
bg_img3 = pygame.image.load("zeleno.png")
bg_img3 = pygame.transform.scale(bg_img3, (WIDTH, HEIGHT))
bg_img4 = pygame.image.load("nista.png")
bg_img4 = pygame.transform.scale(bg_img4, (WIDTH, HEIGHT))
bg_img5 = pygame.image.load("zeleno.png")
bg_img5 = pygame.transform.scale(bg_img5, (WIDTH, HEIGHT))
bg_img6 = pygame.image.load("nista.png")
bg_img6 = pygame.transform.scale(bg_img6, (WIDTH, HEIGHT))
bg_img7 = pygame.image.load("zeleno.png")
bg_img7 = pygame.transform.scale(bg_img7, (WIDTH, HEIGHT))
bg_img8 = pygame.image.load("zuto.png")
bg_img8 = pygame.transform.scale(bg_img8, (WIDTH, HEIGHT))

# tuple velicine prozora
size = (WIDTH, HEIGHT)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Semafor")

clock = pygame.time.Clock()
i = 0
duration = 7000
bg_img = bg_img1
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                bg_color = next_bg(bg_color)
    #i += 1
    if duration < 0 and i == 0:
        duration = 2000
        bg_img = img_next_bg(bg_img)
        i = 1
    elif duration < 0 and i == 1:
        duration = 7000
        bg_img = img_next_bg(bg_img)
        i = 2
    elif duration < 0 and i == 2:
        duration = 700
        bg_img = img_next_bg(bg_img)
        i = 3
    elif duration < 0 and i == 3:
        duration = 700
        bg_img = img_next_bg(bg_img)
        i = 4
    elif duration < 0 and i == 4:
        duration = 700
        bg_img = img_next_bg(bg_img)
        i = 5
    elif duration < 0 and i == 5:
        duration = 700
        bg_img = img_next_bg(bg_img)
        i = 6
    elif duration < 0 and i == 6:
        duration = 2000
        bg_img = img_next_bg(bg_img)
        i = 7
    elif duration < 0 and i == 7:
        duration = 7000
        bg_img = img_next_bg(bg_img)
        i = 0


    screen.blit(bg_img, (0,0))
    pygame.display.flip()
    
    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    duration = duration-time
pygame.quit()
